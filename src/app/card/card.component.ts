import { Component, OnInit } from '@angular/core';
import {CardDto} from '../_models/card-dto';
import {CardService} from '../_services/card.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  cards: CardDto[];

  constructor(
    private cardService: CardService
  ) { }

  ngOnInit(): void {
    this.getCards();
  }

  getCards() {
    this.cardService.getCards().subscribe(resp => {
      this.cards = resp;
      console.log(resp);
    }, error => {
      console.log(error);
    });
  }
}
