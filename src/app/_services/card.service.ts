import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CardDto} from '../_models/card-dto';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class CardService {
  card: CardDto[] = [];

  constructor(private httpClient: HttpClient) {
  }

  getCards(): Observable<CardDto[]> {
    return this.httpClient.get<CardDto[]>(apiUrl + environment.card);
  }
}
