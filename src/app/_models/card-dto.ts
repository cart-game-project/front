export class CardDto {
  cardId: number;
  name: string;
  attack: number;
  defense: number;
  description: string;
}
