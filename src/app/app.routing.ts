import { Routes, RouterModule } from '@angular/router';
import {CardComponent} from './card/card.component';

const appRoutes: Routes = [
  { path: 'card', component: CardComponent},
  { path: '**', redirectTo: 'app-main-menu' }
];

export const routing = RouterModule.forRoot(appRoutes);
